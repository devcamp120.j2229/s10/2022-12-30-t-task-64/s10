package com.devcamp.j64crudapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j64crudapi.model.CVoucher;
public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
}

